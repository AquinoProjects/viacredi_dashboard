import 'package:flutter/material.dart';

class Constantes {
//colors
  static const Color whiteColor = Color(0xffffffff);
  static const Color darkslategrayDefaultContainerColor = Color(0xff103a4c);
  static const Color defaultListContainerColor = Color(0xff0e2a37);
  static const Color defaultDarkslategrayColor = Color(0xff0b2e3e);
  static const Color blackColor = Color(0xff020504);
  static const Color secondWhiteDialogColor = Color(0xffeaedee);
  static const Color greenColor = Color(0xff5ab051);
  static const Color redColor = Color(0xfff44336);
  static const Color lightseagreenColor = Color(0xff13bf9d);
  static const Color lightGrayColor = Color(0xffdbe2e5);
  static const Color darkGrayGradientColor = Color(0xff0c4449);
  static const Color darkslategrayBlocksQuestions = Color(0xff0e3444);

//fontSize
  static const double fontSizeLow = 12; 
  static const double fontSizeBase = 18; 
  static const double fontSizeEmphasis = 25;
  static const double fontSizeBigEmphasis = 45;

//iconSize
  static const double iconSizeBase = 30;
  static const double iconSizeEmphasis = 40;
  static const double iconSizeBigEmphasis = 55;
}
