// ignore_for_file: prefer_const_constructors

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:viacredi_dashboard/models/firestore_db.dart';
import 'package:viacredi_dashboard/utils/constantes.dart';
import 'package:viacredi_dashboard/utils/constantes.dart';
import 'package:viacredi_dashboard/models/firestore_db.dart';

// ignore: use_key_in_widget_constructors
class Login extends StatefulWidget {
  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<Login> {
  @override
  TextEditingController _emailController = new TextEditingController();
  TextEditingController _passwordController = new TextEditingController();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          Container(
            color: Color(0xFF175478),
          ),
          Center(
            child: SingleChildScrollView(
              child: Container(
                height: 380,
                width: 300,
                color: Color(0xFF103a4c),
                child: Column(
                  children: [
                    Image.asset(
                      'images/logo_white.png',
                      width: 250,
                    ),
                    Padding(
                      padding: EdgeInsets.only(left: 20, right: 20),
                      child: TextField(
                        controller: _emailController,
                        cursorColor: Color(0xff0b2e3e),
                        //controller: controllerFields,
                        decoration: InputDecoration(
                          enabledBorder: UnderlineInputBorder(
                            borderSide: BorderSide(
                              color: Color(0xff1d4556),
                              width: 2,
                            ),
                          ),
                          focusedBorder: UnderlineInputBorder(
                            borderSide: BorderSide(
                              color: Color(0xff0b2e3e),
                            ),
                          ),
                          labelText: 'E-mail',
                          labelStyle: TextStyle(color: Color(0xFFFFFFFF)),
                        ),
                        style: TextStyle(
                          color: Color(0xFFFFFFFF),
                          fontSize: 18,
                        ),
                        //onChanged: coinChanged,
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.only(left: 20, right: 20),
                      child: TextField(
                        obscureText: true,
                        controller: _passwordController,
                        cursorColor: Color(0xff0b2e3e),

                        decoration: InputDecoration(
                          enabledBorder: UnderlineInputBorder(
                            borderSide: BorderSide(
                              color: Color(0xff1d4556),
                              width: 2,
                            ),
                          ),
                          focusedBorder: UnderlineInputBorder(
                            borderSide: BorderSide(
                              color: Color(0xff0b2e3e),
                            ),
                          ),
                          labelText: 'Senha',
                          labelStyle: TextStyle(color: Color(0xFFFFFFFF)),
                        ),
                        style: TextStyle(
                          color: Color(0xFFFFFFFF),
                          fontSize: 18,
                        ),
                        //onChanged: coinChanged,
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(top: 15),
                      child: ElevatedButton(
                        style: ElevatedButton.styleFrom(
                          primary: Color(0XFF00bf9e),
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(5),
                          ),
                          padding: EdgeInsets.only(
                              left: 38, top: 15, right: 38, bottom: 15),
                        ),
                        onPressed: () async {
                          login(_emailController.text, _passwordController.text,
                              (validation, error) {
                            if (validation != false) {
                              Navigator.pushNamedAndRemoveUntil(
                                  context, "/DashboardHome", (r) => false);
                            }

                            showDialog<String>(
                              context: context,
                              builder: (BuildContext context) => AlertDialog(
                                backgroundColor:
                                    Constantes.defaultDarkslategrayColor,
                                title: const Text(
                                  'Informação',
                                  style: TextStyle(
                                    color: Constantes.secondWhiteDialogColor,
                                  ),
                                ),
                                content: Text(
                                  error,
                                  //msg,
                                  style: TextStyle(
                                    color: Constantes.secondWhiteDialogColor,
                                  ),
                                ),
                                actions: <Widget>[
                                  TextButton(
                                    style: TextButton.styleFrom(
                                        backgroundColor: Constantes.greenColor),
                                    onPressed: () => Navigator.pop(
                                      context,
                                      'OK',
                                    ),
                                    child: const Text(
                                      'OK',
                                      style: TextStyle(
                                          color: Constantes
                                              .defaultDarkslategrayColor),
                                    ),
                                  ),
                                ],
                              ),
                            );
                          });
                          // ignore: curly_braces_in_flow_control_structures
                        },
                        child: Text(
                          'Login',
                          style: TextStyle(fontSize: 20),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  Future<void> login(String _email, String _senha, calbackLogin) async {
    try {
      await FirebaseAuth.instance
          .signInWithEmailAndPassword(email: _email, password: _senha);
          
      return calbackLogin(true, 'Bem vindo(a)!');
    } on FirebaseAuthException catch (error) {
      String msg = "";
      switch (error.code) {
        case "auth/email-already-exists":
          msg = "O e-mail fornecido já está em uso por outro usuário.";
          break;
        case "auth/invalid-email":
          msg = "O e-mail é inválido";
          break;
        case "wrong-password":
          msg = "E-mail ou senha incorretos!";
          break;
        default:
          msg = "Ocorreu um erro ao tentar fazer login";
      }
      print("loginEmailSenha error.code=${error.code}");
      return calbackLogin(false, msg);
    } catch (e) {
      print("Erro no loginEmailSenha : " + e.toString());
      return calbackLogin(
          false, "Erro no login, verifique os dados informados!");
    }
    
  }
}
