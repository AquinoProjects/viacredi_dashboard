// ignore_for_file: prefer_const_constructors

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/painting.dart';
import 'package:flutter/rendering.dart';
import 'package:jiffy/jiffy.dart';
import 'package:syncfusion_flutter_charts/charts.dart';
import 'package:syncfusion_flutter_charts/sparkcharts.dart';

import 'package:flutter/material.dart';
import 'package:viacredi_dashboard/components/general_search_charts.dart';
import 'package:viacredi_dashboard/models/firestore_db.dart';
import 'package:viacredi_dashboard/utils/constantes.dart';

class DashboardHome extends StatefulWidget {
  @override
  _DashboardHomeState createState() => _DashboardHomeState();
}

class _DashboardHomeState extends State<DashboardHome> {
  bool _hasBeenPressedSearch = true;
  bool _hasBeenPressedBack = false;
  bool _clickOnNavBar = false;

  // ignore: prefer_final_fields

  final int _updateIndex = -1;
  List<Widget> _createListNivelContainer() {
    final List<Widget> containerList = [];
    for (var i = 0; i < 11; i++) {
      Color _color = Color(0xff0e2a37);
      switch (i) {
        case 0:
          _color = Color(0xff5b1f16);
          break;
        case 1:
          _color = Color(0xff942a18);
          break;
        case 2:
          _color = Color(0xffe74529);
          break;
        case 3:
          _color = Color(0xffe73c22);
          break;
        case 4:
          _color = Color(0xffec681c);
          break;
        case 5:
          _color = Color(0xfff39019);
          break;
        case 6:
          _color = Color(0xfffbba18);
          break;
        case 7:
          _color = Color(0xfffcda23);
          break;
        case 8:
          _color = Color(0xffcccc5f);
          break;
        case 9:
          _color = Color(0xff69b436);
          break;
        case 10:
          _color = Color(0xff316e2e);
          break;
        case 11:
          _color = Color(0xff5b1f16);
          break;
      }

      int _countPercet = 0;
      _arrayPercentSearchNote.forEach((nota) {
        if (nota == i) {
          _countPercet++;
        }
      });

      containerList.add(
        Column(
          children: [
            Container(
              decoration: BoxDecoration(
                color: (i <= _mediaSearchNote)
                    ? _color
                    : _color = Constantes.defaultListContainerColor,
                borderRadius: BorderRadius.all(
                  Radius.circular(7),
                ),
              ),
              width: 60,
              height: 60,
              child: Center(
                child: Text(
                  i.toString(),
                  style: TextStyle(
                      color: Constantes.blackColor,
                      fontSize: 30,
                      fontWeight: FontWeight.bold),
                  textAlign: TextAlign.center,
                ),
              ),
            ),
            Column(
              children: [
                (_countPercet > 0)
                    ? Icon(
                        Icons.arrow_drop_up,
                        size: Constantes.iconSizeBase,
                        color: Constantes.greenColor,
                      )
                    : Icon(
                        Icons.arrow_drop_down,
                        size: Constantes.iconSizeBase,
                        color: Constantes.redColor,
                      ),
                Text(
                  ((_arrayPercentSearchNote.isNotEmpty)
                          ? ((_countPercet / _arrayPercentSearchNote.length) *
                                  100)
                              .toStringAsFixed(2)
                          : "0") +
                      "%",
                  style: TextStyle(
                    fontWeight: FontWeight.w300,
                    fontSize: Constantes.fontSizeBase,
                    color: (_countPercet <= 0)
                        ? Constantes.redColor
                        : Constantes.greenColor,
                  ),
                ),
              ],
            ),
          ],
        ),
      );
    }

    return containerList;
  }

  // ignore: prefer_final_fields
  double _updateStarIndex = -1;
  // ignore: prefer_final_fields
  double _secondUpdateStarIndex = -1;
  // ignore: prefer_final_fields
  double _thirdUpdateStartIndex = -1;

  List<Widget> _createListStarsButtons() {
    final List<Widget> starsButtonList = [];
    for (var i = 1; i <= 5; i++) {
      Image _defaultStar = Image.asset(
        'images/estrela_active.png',
        color: Constantes.defaultDarkslategrayColor,
      );

      if (i <= _mediaEmployeeNote)
        // ignore: curly_braces_in_flow_control_structures
        _defaultStar = Image.asset(
          'images/estrela_active.png',
        );

      setState(() {});
      _updateStarIndex = _mediaEmployeeNote;

      int _countPercent = 0;
      _arrayEmployeeNote.forEach((nota) {
        if (nota == i) {
          _countPercent++;
        }
      });
      starsButtonList.add(
        Column(
          children: [
            IconButton(
              mouseCursor: SystemMouseCursors.basic,
              padding: EdgeInsets.only(right: 5, left: 5),
              onPressed: () {},
              icon: _defaultStar,
              iconSize: 35,
            ),
            Text(
              ((_arrayEmployeeNote.isNotEmpty)
                      ? ((_countPercent / _arrayEmployeeNote.length) * 100)
                          .toStringAsFixed(1)
                      : "0") +
                  "%",
              style: TextStyle(
                fontWeight: FontWeight.w300,
                fontSize: Constantes.fontSizeLow,
                color: (_countPercent <= 0)
                    ? Constantes.redColor
                    : Constantes.greenColor,
              ),
            ),
          ],
        ),
      );
    }

    return starsButtonList;
  }

  List<Widget> _createSecondListStarsButtons() {
    final List<Widget> starsButtonList = [];
    for (var i = 1; i <= 5; i++) {
      Image _defaultStar = Image.asset(
        'images/estrela_active.png',
        color: Constantes.defaultDarkslategrayColor,
      );

      if (i <= _mediaEnvironment)
        // ignore: curly_braces_in_flow_control_structures
        _defaultStar = Image.asset('images/estrela_active.png');
      _secondUpdateStarIndex = _mediaEnvironment;

      int _countPercent = 0;
      _arrayEnvironmentNote.forEach((nota) {
        if (nota == i) {
          _countPercent++;
        }
      });
      starsButtonList.add(
        Column(
          children: [
            IconButton(
              mouseCursor: SystemMouseCursors.basic,
              padding: EdgeInsets.only(right: 5, left: 5),
              onPressed: () {},
              icon: _defaultStar,
              iconSize: 35,
            ),
            Text(
              ((_arrayEnvironmentNote.isNotEmpty)
                      ? ((_countPercent / _arrayEnvironmentNote.length) * 100)
                          .toStringAsFixed(1)
                      : "0") +
                  "%",
              style: TextStyle(
                fontWeight: FontWeight.w300,
                fontSize: Constantes.fontSizeLow,
                color: (_countPercent <= 0)
                    ? Constantes.redColor
                    : Constantes.greenColor,
              ),
            ),
          ],
        ),
      );
    }

    return starsButtonList;
  }

  List<Widget> _createThirdListStarsButtons() {
    final List<Widget> starsButtonList = [];
    for (var i = 1; i <= 5; i++) {
      Image _defaultStar = Image.asset(
        'images/estrela_active.png',
        color: Constantes.defaultDarkslategrayColor,
      );

      if (i <= _mediaTimer)
        // ignore: curly_braces_in_flow_control_structures
        _defaultStar = Image.asset('images/estrela_active.png');
      _thirdUpdateStartIndex = _mediaTimer;

      int _countPercent = 0;
      _arrayTimerSearchNote.forEach((nota) {
        if (nota == i) {
          _countPercent++;
        }
      });
      starsButtonList.add(
        Column(
          children: [
            IconButton(
              mouseCursor: SystemMouseCursors.basic,
              padding: EdgeInsets.only(right: 5, left: 5),
              onPressed: () {},
              icon: _defaultStar,
              iconSize: 35,
            ),
            Text(
              ((_arrayTimerSearchNote.isNotEmpty)
                      ? ((_countPercent / _arrayTimerSearchNote.length) * 100)
                          .toStringAsFixed(1)
                      : "0") +
                  "%",
              style: TextStyle(
                fontWeight: FontWeight.w300,
                fontSize: Constantes.fontSizeLow,
                color: (_countPercent <= 0)
                    ? Constantes.redColor
                    : Constantes.greenColor,
              ),
            ),
          ],
        ),
      );
    }

    return starsButtonList;
  }

  List _accessSearchList = [];

  final String _nowDate = Jiffy(DateTime.now()).format("yyyyMMdd");
  int _valueStartDate = int.parse(Jiffy(DateTime.now()).format("yyyyMMdd"));
  int _valueAgency = 0;
  String _valueFilterMacText = '';

  int _totalEmployeeSearchNote = 0;
  int _totalEnvironmentSearchNote = 0;
  int _totalTimerSearchNote = 0;
  int _countEnvironmentNote = 0;
  int _countCommentNote = 0;
  int _countEmployeeNote = 0;
  int _countTimerNote = 0;
  int _totalSearchNote = 0;
  int _countSearchNote = 0;
  double _mediaSearchNote = 0;
  List<double> _arrayPercentSearchNote = [];
  List<double> _arrayEmployeeNote = [];
  List<double> _arrayEnvironmentNote = [];
  List<double> _arrayTimerSearchNote = [];
  double _mediaEmployeeNote = 0;
  double _mediaEnvironment = 0;
  double _mediaTimer = 0;

  List<GeneralSearchCharts> chartsPeriodDate = [
    GeneralSearchCharts(Jiffy(DateTime.now().toString()).Md.toString(), 1)
  ];
  int dateForList = int.parse(Jiffy(DateTime.now()).format("yyyyMMdd"));
  void getDataDb() async {
    _totalEnvironmentSearchNote = 0;
    _arrayEnvironmentNote.clear();
    _countEnvironmentNote = 0;
    _mediaEnvironment = 0;

    _totalTimerSearchNote = 0;
    _arrayTimerSearchNote.clear();
    _mediaTimer = 0;

    _arrayEmployeeNote.clear();
    _countEmployeeNote = 0;
    _totalEmployeeSearchNote = 0;
    _mediaEmployeeNote = 0;

    _countCommentNote = 0;

    _countSearchNote = 0;
    _totalSearchNote = 0;
    _mediaSearchNote = 0;
    _arrayPercentSearchNote.clear();

    dateForList = 0;

    var docs = await FireStoreDb().getData(_valueAgency, _valueFilterMacText,
        _valueStartDate, int.parse(_nowDate));
    _accessSearchList = docs;

    _accessSearchList.forEach((doc) {
      if (doc["comment"] != null) {
        _countCommentNote++;
      }

      if (doc["general-search-employee"] != null) {
        _totalEmployeeSearchNote +=
            int.parse(doc["general-search-employee"].toString());
        _arrayEmployeeNote
            .add(double.parse(doc["general-search-employee"].toString()));
        _countEmployeeNote++;
      }

      if (doc["general-search-environment"] != null) {
        _totalEnvironmentSearchNote +=
            int.parse(doc["general-search-environment"].toString());
        _arrayEnvironmentNote
            .add(double.parse(doc["general-search-environment"].toString()));
        _countEnvironmentNote++;
      }

      if (doc["general-search-time"] != null) {
        _totalTimerSearchNote +=
            int.parse(doc["general-search-time"].toString());
        _arrayTimerSearchNote
            .add(double.parse(doc["general-search-time"].toString()));
        _countTimerNote++;
      }

      if (doc["search-note"] != null) {
        _countSearchNote++;
        _totalSearchNote += int.parse(doc["search-note"].toString());
        _arrayPercentSearchNote
            .add(double.parse(doc["search-note"].toString()));
      }

      if (chartsPeriodDate
          .map((e) => e.periodDate)
          .contains(Jiffy(doc["date"].toString()).Md.toString())) {
        int indexR = chartsPeriodDate.indexWhere((element) => element.periodDate
            .contains(Jiffy(doc["date"].toString()).Md.toString()));
        chartsPeriodDate[indexR].countAcess++;
      } else {
        chartsPeriodDate.add(GeneralSearchCharts(
            Jiffy(doc["date"].toString()).Md.toString(), 1));
      }
      /*
      if (chartsPeriodDate != (doc["date"])) {
        chartsPeriodDate.add(GeneralSearchCharts(
            Jiffy(_nowDate).format("yyyy-MM-dd").toString(),
            _accessSearchList.length));

        print("TESTE: $dateForList ${chartsPeriodDate.toString()}");

        /*chartsPeriodDate.forEach((date) {
          if (date != (doc["date"])) {
            
          }
        });*/

      }*/
    });

    if (_totalSearchNote > 0) {
      _mediaSearchNote = (_totalSearchNote / _accessSearchList.length);
    }

    if (_totalEmployeeSearchNote > 0) {
      _mediaEmployeeNote = (_totalEmployeeSearchNote / _countEmployeeNote);
    }

    if (_totalEnvironmentSearchNote > 0) {
      _mediaEnvironment = (_totalEnvironmentSearchNote / _countEnvironmentNote);
    }

    if (_totalTimerSearchNote > 0) {
      _mediaTimer = (_totalTimerSearchNote / _countTimerNote);
    }
    print('_mediaTimer ' + _mediaTimer.toString());
    print("t2: " + chartsPeriodDate.length.toString());

    setState(() {});
  }

  int substractDate(String dateFormat, int substractDays) {
    return int.parse(Jiffy().subtract(days: substractDays).format("yyyyMMdd"));
  }

  @override
  void initState() {
    super.initState();
    if (FirebaseAuth.instance.currentUser != null) {
      Future.delayed(Duration(milliseconds: 100), () {
        getDataDb();
        _hassPressedSearchDaily = true;
        _hassPressedCommentDaily = true;
      });
    } else {
      Navigator.pushNamedAndRemoveUntil(context, "/", (r) => false);
    }
  }

  bool _hassPressedSearchDaily = false;
  bool _hassPressedSearchLastWeek = false;
  bool _hassPressedSearchLastMonth = false;
  bool _hassPressedSearchYesterday = false;

  bool _hassPressedCommentDaily = false;
  bool _hassPressedCommentLastWeek = false;
  bool _hassPressedCommentLastMonth = false;
  bool _hassPressedCommentYesterday = false;

  @override
  Widget build(BuildContext context) {
    Size _screenSize = MediaQuery.of(context).size;
    return Scaffold(
      body: GestureDetector(
        onTap: () {
          setState(() {
            _clickOnNavBar = false;
          });
        },
        child: Container(
          width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.height,
          color: Constantes.defaultDarkslategrayColor,
          child: Column(
            children: [
              //topbar
              Container(
                width: double.maxFinite,
                decoration: BoxDecoration(
                  gradient: LinearGradient(
                      // ignore: prefer_const_literals_to_create_immutables
                      colors: [
                        Constantes.defaultDarkslategrayColor,
                        Constantes.darkGrayGradientColor,
                      ]),
                  border: Border(
                    bottom: BorderSide(
                      color: Color(0XFF01bdb9),
                      width: 2,
                    ),
                  ),
                ),
                child: Wrap(
                  alignment: WrapAlignment.spaceAround,
                  crossAxisAlignment: WrapCrossAlignment.center,
                  children: [
                    Image.asset(
                      'images/logo_white.png',
                      width: 180,
                    ),
                    Padding(
                      padding: const EdgeInsets.all(15.0),
                      child: Wrap(
                        spacing: 50,
                        runSpacing: 30,
                        alignment: WrapAlignment.center,
                        crossAxisAlignment: WrapCrossAlignment.center,
                        children: [
                          Wrap(
                            crossAxisAlignment: WrapCrossAlignment.center,
                            spacing: 10,
                            children: [
                              Text(
                                'Dispositivos:',
                                style: TextStyle(
                                  fontSize: 20,
                                  color: Color(0xFFecf0f1),
                                ),
                              ),
                              Container(
                                padding: EdgeInsets.only(left: 15, right: 15),
                                height: 40,
                                decoration: BoxDecoration(
                                  border: Border.all(
                                      width: 1,
                                      color: Constantes.lightGrayColor),
                                  borderRadius: BorderRadius.circular(7),
                                  // ignore: prefer_const_literals_to_create_immutables
                                  gradient: LinearGradient(colors: [
                                    Constantes.defaultDarkslategrayColor,
                                    Constantes
                                        .darkslategrayDefaultContainerColor,
                                  ]),
                                ),
                                child: DropdownButtonHideUnderline(
                                  child: DropdownButton(
                                    alignment: Alignment.center,
                                    borderRadius: BorderRadius.circular(7),
                                    dropdownColor: Constantes
                                        .darkslategrayDefaultContainerColor,
                                    iconEnabledColor: Constantes.whiteColor,
                                    iconDisabledColor: Constantes
                                        .darkslategrayDefaultContainerColor,
                                    value: _valueFilterMacText,
                                    onChanged: (String? newValDevices) {
                                      setState(() {
                                        _clickOnNavBar = false;
                                        _valueFilterMacText = newValDevices!;
                                      });
                                      getDataDb();
                                    },
                                    // ignore: prefer_const_literals_to_create_immutables
                                    items: [
                                      DropdownMenuItem(
                                        alignment: Alignment.center,
                                        value: '',
                                        child: Text(
                                          'Todos',
                                          style: TextStyle(
                                              color: Color(0xFFecf0f1)),
                                        ),
                                      ),
                                      DropdownMenuItem(
                                        alignment: Alignment.center,
                                        value: 'asdasd',
                                        child: Text(
                                          'asdasd',
                                          style: TextStyle(
                                              color: Color(0xFFecf0f1)),
                                        ),
                                      ),
                                      DropdownMenuItem(
                                        alignment: Alignment.center,
                                        value: 'hhhgh',
                                        child: Text(
                                          'hhhgh',
                                          style: TextStyle(
                                              color: Color(0xFFecf0f1)),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ],
                          ),
                          Wrap(
                            crossAxisAlignment: WrapCrossAlignment.center,
                            spacing: 10,
                            children: [
                              Text(
                                'Agência:',
                                style: TextStyle(
                                  fontSize: 20,
                                  color: Color(0xFFecf0f1),
                                ),
                              ),
                              Container(
                                padding: EdgeInsets.only(left: 15, right: 15),
                                height: 40,
                                decoration: BoxDecoration(
                                  border: Border.all(
                                      width: 1,
                                      color: Constantes.lightGrayColor),
                                  borderRadius: BorderRadius.circular(7),
                                  // ignore: prefer_const_literals_to_create_immutables
                                  gradient: LinearGradient(colors: [
                                    Constantes.defaultDarkslategrayColor,
                                    Constantes
                                        .darkslategrayDefaultContainerColor,
                                  ]),
                                ),
                                child: DropdownButtonHideUnderline(
                                  child: DropdownButton(
                                    alignment: Alignment.center,
                                    borderRadius: BorderRadius.circular(7),
                                    dropdownColor: Constantes
                                        .darkslategrayDefaultContainerColor,
                                    iconEnabledColor: Constantes.whiteColor,
                                    iconDisabledColor: Constantes
                                        .darkslategrayDefaultContainerColor,
                                    value: _valueAgency,
                                    onChanged: (int? newValAgency) {
                                      setState(() {
                                        _clickOnNavBar = false;
                                        _valueAgency = newValAgency!;
                                      });
                                      getDataDb();
                                    },
                                    // ignore: prefer_const_literals_to_create_immutables
                                    items: [
                                      DropdownMenuItem(
                                        alignment: Alignment.center,
                                        value: 0,
                                        child: Text(
                                          'Todos',
                                          style: TextStyle(
                                              color: Color(0xFFecf0f1)),
                                        ),
                                      ),
                                      DropdownMenuItem(
                                        alignment: Alignment.center,
                                        value: 8057,
                                        child: Text(
                                          '8057',
                                          style: TextStyle(
                                              color: Color(0xFFecf0f1)),
                                        ),
                                      ),
                                      DropdownMenuItem(
                                        alignment: Alignment.center,
                                        value: 8058,
                                        child: Text(
                                          '8058',
                                          style: TextStyle(
                                              color: Color(0xFFecf0f1)),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ],
                          ),
                          Wrap(
                            crossAxisAlignment: WrapCrossAlignment.center,
                            spacing: 10,
                            children: [
                              Text(
                                'Período:',
                                style: TextStyle(
                                  fontSize: 20,
                                  color: Color(0xFFecf0f1),
                                ),
                              ),
                              Container(
                                padding: EdgeInsets.only(left: 15, right: 15),
                                height: 40,
                                decoration: BoxDecoration(
                                  border: Border.all(
                                      width: 1,
                                      color: Constantes.lightGrayColor),
                                  borderRadius: BorderRadius.circular(7),
                                  // ignore: prefer_const_literals_to_create_immutables
                                  gradient: LinearGradient(colors: [
                                    Constantes.defaultDarkslategrayColor,
                                    Constantes
                                        .darkslategrayDefaultContainerColor,
                                  ]),
                                ),
                                child: DropdownButtonHideUnderline(
                                  child: DropdownButton(
                                    alignment: Alignment.center,
                                    borderRadius: BorderRadius.circular(7),
                                    dropdownColor: Constantes
                                        .darkslategrayDefaultContainerColor,
                                    iconEnabledColor: Constantes.whiteColor,
                                    iconDisabledColor: Constantes
                                        .darkslategrayDefaultContainerColor,
                                    value: _valueStartDate,
                                    onChanged: (int? newValDate) {
                                      setState(() {
                                        _clickOnNavBar = false;
                                        _valueStartDate = newValDate!;

                                        if (_valueStartDate ==
                                            substractDate(_nowDate, 0)) {
                                          _hassPressedSearchDaily = true;
                                          _hassPressedSearchLastWeek = false;
                                          _hassPressedSearchLastMonth = false;
                                          _hassPressedSearchYesterday = false;

                                          _hassPressedCommentYesterday = false;
                                          _hassPressedCommentDaily = true;
                                          _hassPressedCommentLastWeek = false;
                                          _hassPressedCommentLastMonth = false;
                                        } else if (_valueStartDate ==
                                            substractDate(_nowDate, 1)) {
                                          _hassPressedSearchLastWeek = false;
                                          _hassPressedSearchDaily = false;
                                          _hassPressedSearchLastMonth = false;
                                          _hassPressedSearchYesterday = true;

                                          _hassPressedCommentYesterday = true;
                                          _hassPressedCommentDaily = false;
                                          _hassPressedCommentLastWeek = false;
                                          _hassPressedCommentLastMonth = false;
                                        } else if (_valueStartDate ==
                                            substractDate(_nowDate, 7)) {
                                          _hassPressedSearchLastWeek = true;
                                          _hassPressedSearchDaily = false;
                                          _hassPressedSearchLastMonth = false;
                                          _hassPressedSearchYesterday = false;

                                          _hassPressedCommentYesterday = false;
                                          _hassPressedCommentDaily = false;
                                          _hassPressedCommentLastWeek = true;
                                          _hassPressedCommentLastMonth = false;
                                        } else if (_valueStartDate ==
                                            substractDate(_nowDate, 30)) {
                                          _hassPressedSearchLastMonth = true;
                                          _hassPressedSearchLastWeek = false;
                                          _hassPressedSearchDaily = false;
                                          _hassPressedSearchYesterday = false;

                                          _hassPressedCommentYesterday = false;
                                          _hassPressedCommentDaily = false;
                                          _hassPressedCommentLastWeek = false;
                                          _hassPressedCommentLastMonth = true;
                                        }
                                      });
                                      getDataDb();
                                    },
                                    // ignore: prefer_const_literals_to_create_immutables
                                    items: [
                                      DropdownMenuItem(
                                        alignment: Alignment.center,
                                        value: substractDate(_nowDate, 0),
                                        child: Text(
                                          'Hoje',
                                          style: TextStyle(
                                              color: Color(0xFFecf0f1)),
                                        ),
                                      ),
                                      DropdownMenuItem(
                                        alignment: Alignment.center,
                                        value: substractDate(_nowDate, 1),
                                        child: Text(
                                          'Ontem',
                                          style: TextStyle(
                                              color: Color(0xFFecf0f1)),
                                        ),
                                      ),
                                      DropdownMenuItem(
                                        alignment: Alignment.center,
                                        value: substractDate(_nowDate, 7),
                                        child: Text(
                                          'Últimos 7 Dias',
                                          style: TextStyle(
                                              color: Color(0xFFecf0f1)),
                                        ),
                                      ),
                                      DropdownMenuItem(
                                        alignment: Alignment.center,
                                        value: substractDate(_nowDate, 30),
                                        child: Text(
                                          'Últimos 30 Dias',
                                          style: TextStyle(
                                              color: Color(0xFFecf0f1)),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
              Expanded(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    (_screenSize.width <= 700)
                        ? Container()
                        : Container(
                            color: Color(0XFF0d2836),
                            width: 95,
                            height: double.maxFinite,
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: [
                                Container(
                                  width: 95,
                                  height: 108,
                                  // ignore: unrelated_type_equality_checks
                                  color: _hasBeenPressedSearch
                                      ? Constantes
                                          .darkslategrayDefaultContainerColor
                                      : Color(0XFF0d2836),

                                  child: IconButton(
                                    tooltip: 'Totem',
                                    onPressed: () {
                                      setState(() {
                                        _hasBeenPressedSearch = true;

                                        if (_hasBeenPressedSearch == true) {
                                          _hasBeenPressedBack = false;
                                        }
                                      });
                                    },
                                    icon: Icon(
                                      Icons.search_sharp,
                                      size: 45,
                                      color: Constantes.greenColor,
                                    ),
                                  ),
                                ),
                                Container(
                                  width: 95,
                                  height: 108,
                                  color: _hasBeenPressedBack
                                      ? Constantes
                                          .darkslategrayDefaultContainerColor
                                      : Color(0XFF0d2836),
                                  child: IconButton(
                                    tooltip: 'Sair',
                                    onPressed: () async {
                                      setState(() {
                                        _hasBeenPressedBack = true;
                                        if (_hasBeenPressedBack == true) {
                                          _hasBeenPressedSearch = false;
                                        }
                                        showDialog<String>(
                                          context: context,
                                          builder: (BuildContext context) =>
                                              AlertDialog(
                                            backgroundColor: Constantes
                                                .defaultDarkslategrayColor,
                                            title: const Text(
                                              'Informação',
                                              style: TextStyle(
                                                color: Constantes
                                                    .secondWhiteDialogColor,
                                              ),
                                            ),
                                            content: const Text(
                                              'Deseja realmente sair?',
                                              style: TextStyle(
                                                color: Constantes
                                                    .secondWhiteDialogColor,
                                              ),
                                            ),
                                            actions: <Widget>[
                                              TextButton(
                                                style: TextButton.styleFrom(
                                                    backgroundColor:
                                                        Constantes.greenColor),
                                                onPressed: () async {
                                                  await FirebaseAuth.instance
                                                      .signOut();
                                                  Navigator
                                                      .pushNamedAndRemoveUntil(
                                                          context,
                                                          "/",
                                                          (r) => false);
                                                },
                                                child: const Text(
                                                  'Sair',
                                                  style: TextStyle(
                                                      color: Constantes
                                                          .defaultDarkslategrayColor),
                                                ),
                                              ),
                                              TextButton(
                                                style: TextButton.styleFrom(
                                                    backgroundColor:
                                                        Constantes.greenColor),
                                                onPressed: () {
                                                  Navigator
                                                      .pushNamedAndRemoveUntil(
                                                          context,
                                                          "/DashboardHome",
                                                          (r) => false);
                                                },
                                                child: const Text(
                                                  'Cancelar',
                                                  style: TextStyle(
                                                      color: Constantes
                                                          .defaultDarkslategrayColor),
                                                ),
                                              ),
                                            ],
                                          ),
                                        );
                                      });
                                    },
                                    icon: Icon(
                                      Icons.power_settings_new_sharp,
                                      size: 45,
                                      color: Constantes.greenColor,
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                    Expanded(
                      child: SingleChildScrollView(
                        child: Padding(
                          padding: const EdgeInsets.only(
                              top: 5.0, left: 15, right: 15),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              Wrap(
                                alignment: WrapAlignment.center,
                                spacing: 20,
                                runSpacing: 20,
                                children: [
                                  (_screenSize.width <= 700)
                                      ? Padding(
                                          padding:
                                              const EdgeInsets.only(right: 17),
                                          child: Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.spaceBetween,
                                            children: [
                                              IconButton(
                                                  onPressed: () {
                                                    setState(() {
                                                      _clickOnNavBar =
                                                          !_clickOnNavBar;
                                                    });
                                                  },
                                                  icon: Icon(
                                                    Icons.blur_on_outlined,
                                                    color:
                                                        Constantes.greenColor,
                                                    size: 50,
                                                  )),
                                              (_clickOnNavBar != false &&
                                                      _screenSize.width <= 700)
                                                  ? Wrap(
                                                      spacing: 50,
                                                      children: [
                                                        IconButton(
                                                          alignment:
                                                              Alignment.center,
                                                          tooltip: 'Totem',
                                                          onPressed: () {
                                                            setState(() {
                                                              _hasBeenPressedSearch =
                                                                  true;

                                                              if (_hasBeenPressedSearch ==
                                                                  true) {
                                                                _hasBeenPressedBack =
                                                                    false;
                                                              }
                                                            });
                                                          },
                                                          icon: Icon(
                                                            Icons.search_sharp,
                                                            size: 45,
                                                            color: Constantes
                                                                .greenColor,
                                                          ),
                                                        ),
                                                        IconButton(
                                                          tooltip: 'Sair',
                                                          onPressed: () {
                                                            setState(() {
                                                              _hasBeenPressedBack =
                                                                  true;
                                                              if (_hasBeenPressedBack ==
                                                                  true) {
                                                                _hasBeenPressedSearch =
                                                                    false;
                                                              }
                                                            });
                                                            Navigator
                                                                .pushNamedAndRemoveUntil(
                                                                    context,
                                                                    "/",
                                                                    (r) =>
                                                                        false);
                                                          },
                                                          icon: Icon(
                                                            Icons
                                                                .power_settings_new_sharp,
                                                            size: 45,
                                                            color: Constantes
                                                                .greenColor,
                                                          ),
                                                        ),
                                                      ],
                                                    )
                                                  : Container()
                                            ],
                                          ),
                                        )
                                      : Container(),
                                  Wrap(
                                    children: [
                                      Wrap(
                                        children: [
                                          Container(
                                            height: 333,
                                            color: Constantes
                                                .darkslategrayDefaultContainerColor,
                                            child: Column(
                                              children: [
                                                // ignore: sized_box_for_whitespace
                                                Container(
                                                  width: 255,
                                                  child: Padding(
                                                    padding:
                                                        const EdgeInsets.only(
                                                            top: 5, bottom: 5),
                                                    child: Row(
                                                      mainAxisAlignment:
                                                          MainAxisAlignment
                                                              .spaceBetween,
                                                      // ignore: prefer_const_literals_to_create_immutables
                                                      children: [
                                                        Icon(
                                                          Icons.search,
                                                          color: Constantes
                                                              .greenColor,
                                                        ),
                                                        Text(
                                                          'Total de Pesquisas',
                                                          style: TextStyle(
                                                            color: Constantes
                                                                .lightGrayColor,
                                                            fontSize: Constantes
                                                                .fontSizeBase,
                                                          ),
                                                        ),
                                                        InkWell(
                                                          child: Icon(
                                                            Icons
                                                                .library_add_check_outlined,
                                                            size: 20,
                                                            color: Constantes
                                                                .lightGrayColor,
                                                          ),
                                                          onTap: () {
                                                            showDialog<String>(
                                                              context: context,
                                                              builder: (BuildContext
                                                                      context) =>
                                                                  AlertDialog(
                                                                backgroundColor:
                                                                    Constantes
                                                                        .defaultDarkslategrayColor,
                                                                title:
                                                                    const Text(
                                                                  'Informação',
                                                                  style:
                                                                      TextStyle(
                                                                    color: Constantes
                                                                        .secondWhiteDialogColor,
                                                                  ),
                                                                ),
                                                                content:
                                                                    const Text(
                                                                  'Quantidade total de pesquisas respondidas no período selecionado.',
                                                                  style:
                                                                      TextStyle(
                                                                    color: Constantes
                                                                        .secondWhiteDialogColor,
                                                                  ),
                                                                ),
                                                                actions: <
                                                                    Widget>[
                                                                  TextButton(
                                                                    style: TextButton.styleFrom(
                                                                        backgroundColor:
                                                                            Constantes.greenColor),
                                                                    onPressed: () =>
                                                                        Navigator
                                                                            .pop(
                                                                      context,
                                                                      'OK',
                                                                    ),
                                                                    child:
                                                                        const Text(
                                                                      'OK',
                                                                      style: TextStyle(
                                                                          color:
                                                                              Constantes.defaultDarkslategrayColor),
                                                                    ),
                                                                  ),
                                                                ],
                                                              ),
                                                            );
                                                          },
                                                        ),
                                                      ],
                                                    ),
                                                  ),
                                                ),
                                                Container(
                                                    width: 255,
                                                    height: 1,
                                                    color: Constantes
                                                        .defaultDarkslategrayColor),
                                                // ignore: sized_box_for_whitespace
                                                Container(
                                                  alignment: Alignment.center,
                                                  height: 150,
                                                  width: 266,
                                                  child: Row(
                                                    mainAxisAlignment:
                                                        MainAxisAlignment
                                                            .center,
                                                    children: [
                                                      Text(
                                                        "${_accessSearchList.length}",
                                                        style: TextStyle(
                                                            color: Constantes
                                                                .lightGrayColor,
                                                            fontSize: Constantes
                                                                .fontSizeBigEmphasis),
                                                        textAlign:
                                                            TextAlign.center,
                                                      ),
                                                      (_accessSearchList
                                                                  .length >
                                                              0)
                                                          ? Icon(
                                                              Icons
                                                                  .arrow_drop_up,
                                                              size: 55,
                                                              color: Constantes
                                                                  .greenColor,
                                                            )
                                                          : Icon(
                                                              Icons
                                                                  .arrow_drop_down,
                                                              size: Constantes
                                                                  .iconSizeEmphasis,
                                                              color: Constantes
                                                                  .redColor,
                                                            ),
                                                      Text(
                                                        "100%",
                                                        style: TextStyle(
                                                          fontWeight:
                                                              FontWeight.bold,
                                                          fontSize: Constantes
                                                              .fontSizeEmphasis,
                                                          color: (_accessSearchList
                                                                      .length <=
                                                                  0)
                                                              ? Constantes
                                                                  .redColor
                                                              : Constantes
                                                                  .greenColor,
                                                        ),
                                                      ),
                                                    ],
                                                  ),
                                                ),
                                                Container(
                                                  width: 255,
                                                  height: 1,
                                                  color: Constantes
                                                      .defaultDarkslategrayColor,
                                                ),

                                                Padding(
                                                  padding:
                                                      const EdgeInsets.only(
                                                          top: 35),
                                                  child: Container(
                                                    width: 255,
                                                    child: Column(
                                                      children: [
                                                        (_hassPressedSearchDaily !=
                                                                false)
                                                            ? Row(
                                                                mainAxisAlignment:
                                                                    MainAxisAlignment
                                                                        .spaceBetween,
                                                                children: [
                                                                  RichText(
                                                                    text:
                                                                        TextSpan(
                                                                      style: TextStyle(
                                                                          fontSize: Constantes
                                                                              .fontSizeBase,
                                                                          color:
                                                                              Constantes.lightGrayColor),
                                                                      // ignore: prefer_const_literals_to_create_immutables
                                                                      children: <
                                                                          TextSpan>[
                                                                        TextSpan(
                                                                            text:
                                                                                'Total '),
                                                                        TextSpan(
                                                                            text:
                                                                                'Diário',
                                                                            style:
                                                                                TextStyle(color: Constantes.lightseagreenColor)),
                                                                      ],
                                                                    ),
                                                                  ),
                                                                  Row(
                                                                    children: [
                                                                      Text(
                                                                        "${_accessSearchList.length}",
                                                                        style: TextStyle(
                                                                            color:
                                                                                Constantes.lightGrayColor,
                                                                            fontSize: Constantes.fontSizeBase),
                                                                        textAlign:
                                                                            TextAlign.center,
                                                                      ),
                                                                      (_accessSearchList.length >
                                                                              0)
                                                                          ? Icon(
                                                                              Icons.arrow_drop_up,
                                                                              size: Constantes.iconSizeBase,
                                                                              color: Constantes.greenColor,
                                                                            )
                                                                          : Icon(
                                                                              Icons.arrow_drop_down,
                                                                              size: Constantes.iconSizeBase,
                                                                              color: Constantes.redColor,
                                                                            ),
                                                                      Text(
                                                                        "100%",
                                                                        style:
                                                                            TextStyle(
                                                                          fontWeight:
                                                                              FontWeight.bold,
                                                                          fontSize:
                                                                              Constantes.fontSizeBase,
                                                                          color: (_accessSearchList.length <= 0)
                                                                              ? Constantes.redColor
                                                                              : Constantes.greenColor,
                                                                        ),
                                                                      ),
                                                                    ],
                                                                  ),
                                                                ],
                                                              )
                                                            : Container(),
                                                        (_hassPressedSearchYesterday !=
                                                                false)
                                                            ? Row(
                                                                mainAxisAlignment:
                                                                    MainAxisAlignment
                                                                        .spaceBetween,
                                                                children: [
                                                                  RichText(
                                                                    text:
                                                                        TextSpan(
                                                                      style: TextStyle(
                                                                          fontSize: Constantes
                                                                              .fontSizeBase,
                                                                          color:
                                                                              Constantes.lightGrayColor),
                                                                      // ignore: prefer_const_literals_to_create_immutables
                                                                      children: <
                                                                          TextSpan>[
                                                                        TextSpan(
                                                                            text:
                                                                                'Total '),
                                                                        TextSpan(
                                                                            text:
                                                                                'Dia Anterior',
                                                                            style:
                                                                                TextStyle(color: Constantes.lightseagreenColor)),
                                                                      ],
                                                                    ),
                                                                  ),
                                                                  Row(
                                                                    children: [
                                                                      Text(
                                                                        "${_accessSearchList.length}",
                                                                        style: TextStyle(
                                                                            color:
                                                                                Constantes.lightGrayColor,
                                                                            fontSize: Constantes.fontSizeBase),
                                                                        textAlign:
                                                                            TextAlign.center,
                                                                      ),
                                                                      (_accessSearchList.length >
                                                                              0)
                                                                          ? Icon(
                                                                              Icons.arrow_drop_up,
                                                                              size: Constantes.iconSizeBase,
                                                                              color: Constantes.greenColor,
                                                                            )
                                                                          : Icon(
                                                                              Icons.arrow_drop_down,
                                                                              size: Constantes.iconSizeBase,
                                                                              color: Constantes.redColor,
                                                                            ),
                                                                      Text(
                                                                        "100%",
                                                                        style:
                                                                            TextStyle(
                                                                          fontWeight:
                                                                              FontWeight.bold,
                                                                          fontSize:
                                                                              Constantes.fontSizeBase,
                                                                          color: (_accessSearchList.length <= 0)
                                                                              ? Constantes.redColor
                                                                              : Constantes.greenColor,
                                                                        ),
                                                                      ),
                                                                    ],
                                                                  ),
                                                                ],
                                                              )
                                                            : Container(),
                                                        (_hassPressedSearchLastWeek !=
                                                                false)
                                                            ? Row(
                                                                mainAxisAlignment:
                                                                    MainAxisAlignment
                                                                        .spaceBetween,
                                                                children: [
                                                                  RichText(
                                                                    text:
                                                                        TextSpan(
                                                                      style:
                                                                          TextStyle(
                                                                        fontSize:
                                                                            Constantes.fontSizeBase,
                                                                        color: Constantes
                                                                            .lightGrayColor,
                                                                      ),
                                                                      // ignore: prefer_const_literals_to_create_immutables
                                                                      children: <
                                                                          TextSpan>[
                                                                        TextSpan(
                                                                            text:
                                                                                'Total '),
                                                                        TextSpan(
                                                                            text:
                                                                                'Últimos 7 dias ',
                                                                            style:
                                                                                TextStyle(color: Constantes.lightseagreenColor)),
                                                                      ],
                                                                    ),
                                                                  ),
                                                                  Row(
                                                                    children: [
                                                                      Text(
                                                                        "${_accessSearchList.length}",
                                                                        style: TextStyle(
                                                                            color:
                                                                                Constantes.whiteColor,
                                                                            fontSize: Constantes.fontSizeBase),
                                                                        textAlign:
                                                                            TextAlign.center,
                                                                      ),
                                                                      (_accessSearchList.length >
                                                                              0)
                                                                          ? Icon(
                                                                              Icons.arrow_drop_up,
                                                                              size: Constantes.iconSizeBase,
                                                                              color: Constantes.greenColor,
                                                                            )
                                                                          : Icon(
                                                                              Icons.arrow_drop_down,
                                                                              size: Constantes.iconSizeBase,
                                                                              color: Constantes.redColor,
                                                                            ),
                                                                      Text(
                                                                        "100%",
                                                                        style:
                                                                            TextStyle(
                                                                          fontWeight:
                                                                              FontWeight.bold,
                                                                          fontSize:
                                                                              Constantes.fontSizeBase,
                                                                          color: (_accessSearchList.length <= 0)
                                                                              ? Constantes.redColor
                                                                              : Constantes.greenColor,
                                                                        ),
                                                                      ),
                                                                    ],
                                                                  ),
                                                                ],
                                                              )
                                                            : Container(),
                                                        (_hassPressedSearchLastMonth !=
                                                                false)
                                                            ? Row(
                                                                mainAxisAlignment:
                                                                    MainAxisAlignment
                                                                        .spaceBetween,
                                                                children: [
                                                                  RichText(
                                                                    text:
                                                                        TextSpan(
                                                                      style:
                                                                          TextStyle(
                                                                        fontSize:
                                                                            Constantes.fontSizeBase,
                                                                        color: Constantes
                                                                            .lightGrayColor,
                                                                      ),
                                                                      // ignore: prefer_const_literals_to_create_immutables
                                                                      children: <
                                                                          TextSpan>[
                                                                        TextSpan(
                                                                            text:
                                                                                'Total '),
                                                                        TextSpan(
                                                                            text:
                                                                                'Mensal',
                                                                            style:
                                                                                TextStyle(color: Constantes.lightseagreenColor)),
                                                                      ],
                                                                    ),
                                                                  ),
                                                                  Row(
                                                                    children: [
                                                                      Text(
                                                                        "${_accessSearchList.length}",
                                                                        style: TextStyle(
                                                                            color:
                                                                                Constantes.lightGrayColor,
                                                                            fontSize: Constantes.fontSizeBase),
                                                                        textAlign:
                                                                            TextAlign.center,
                                                                      ),
                                                                      (_accessSearchList.length >
                                                                              0)
                                                                          ? Icon(
                                                                              Icons.arrow_drop_up,
                                                                              size: Constantes.iconSizeBase,
                                                                              color: Constantes.greenColor,
                                                                            )
                                                                          : Icon(
                                                                              Icons.arrow_drop_down,
                                                                              size: Constantes.iconSizeBase,
                                                                              color: Constantes.redColor,
                                                                            ),
                                                                      Text(
                                                                        "100%",
                                                                        style: TextStyle(
                                                                            fontWeight: FontWeight
                                                                                .bold,
                                                                            fontSize: Constantes
                                                                                .fontSizeBase,
                                                                            color: (_accessSearchList.length <= 0)
                                                                                ? Constantes.redColor
                                                                                : Constantes.greenColor),
                                                                      ),
                                                                    ],
                                                                  ),
                                                                ],
                                                              )
                                                            : Container(),
                                                      ],
                                                    ),
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ),
                                        ],
                                      ),
                                    ],
                                  ),
                                  Wrap(
                                    children: [
                                      Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.center,
                                        children: [
                                          Container(
                                            color: Constantes
                                                .darkslategrayDefaultContainerColor,
                                            width: 930,
                                            child: Column(
                                              children: [
                                                Padding(
                                                  padding:
                                                      const EdgeInsets.all(6.5),
                                                  child: Row(
                                                    mainAxisAlignment:
                                                        MainAxisAlignment
                                                            .spaceBetween,
                                                    // ignore: prefer_const_literals_to_create_immutables
                                                    children: [
                                                      Icon(
                                                        Icons
                                                            .access_time_outlined,
                                                        size: 20,
                                                        color: Constantes
                                                            .whiteColor,
                                                      ),
                                                      Text(
                                                        'Horários de Utilização [ Dia ]',
                                                        style: TextStyle(
                                                          color: Constantes
                                                              .lightGrayColor,
                                                          fontSize: Constantes
                                                              .fontSizeBase,
                                                        ),
                                                      ),
                                                      InkWell(
                                                        child: Icon(
                                                          Icons
                                                              .library_add_check_outlined,
                                                          size: 20,
                                                          color: Constantes
                                                              .lightGrayColor,
                                                        ),
                                                        onTap: () {
                                                          showDialog<String>(
                                                            context: context,
                                                            builder: (BuildContext
                                                                    context) =>
                                                                AlertDialog(
                                                              backgroundColor:
                                                                  Constantes
                                                                      .defaultDarkslategrayColor,
                                                              title: const Text(
                                                                'Informação',
                                                                style:
                                                                    TextStyle(
                                                                  color: Constantes
                                                                      .secondWhiteDialogColor,
                                                                ),
                                                              ),
                                                              content:
                                                                  const Text(
                                                                'Dias com maior quantidade de pesquisas realizadas durante o período selecionado.',
                                                                style:
                                                                    TextStyle(
                                                                  color: Constantes
                                                                      .secondWhiteDialogColor,
                                                                ),
                                                              ),
                                                              actions: <Widget>[
                                                                TextButton(
                                                                  style: TextButton.styleFrom(
                                                                      backgroundColor:
                                                                          Constantes
                                                                              .greenColor),
                                                                  onPressed: () =>
                                                                      Navigator
                                                                          .pop(
                                                                    context,
                                                                    'OK',
                                                                  ),
                                                                  child:
                                                                      const Text(
                                                                    'OK',
                                                                    style: TextStyle(
                                                                        color: Constantes
                                                                            .defaultDarkslategrayColor),
                                                                  ),
                                                                ),
                                                              ],
                                                            ),
                                                          );
                                                        },
                                                      ),
                                                    ],
                                                  ),
                                                ),
                                                Container(
                                                  width: 930,
                                                  height: 1,
                                                  color: Constantes
                                                      .defaultDarkslategrayColor,
                                                ),
                                                Container(
                                                  color: Constantes
                                                      .darkslategrayDefaultContainerColor,
                                                  width: 930,
                                                  child: Container(
                                                    width: 930,
                                                    child: Column(children: [
                                                      //Initialize the chart widget
                                                      SfCartesianChart(
                                                          plotAreaBorderWidth:
                                                              0,
                                                          primaryXAxis:
                                                              CategoryAxis(
                                                            majorGridLines:
                                                                MajorGridLines(
                                                                    width: 0),
                                                            axisLine: AxisLine(
                                                                width: 0),
                                                          ),

                                                          // Enable legend
                                                          legend: Legend(
                                                              isVisible: false),
                                                          // Enable tooltip
                                                          tooltipBehavior:
                                                              TooltipBehavior(
                                                                  enable: true),
                                                          series: <
                                                              ChartSeries<
                                                                  GeneralSearchCharts,
                                                                  String>>[
                                                            SplineSeries<
                                                                GeneralSearchCharts,
                                                                String>(
                                                              color: Constantes
                                                                  .greenColor,
                                                              name: 'Diário',
                                                              dataSource:
                                                                  chartsPeriodDate,
                                                              splineType:
                                                                  SplineType
                                                                      .cardinal,
                                                              cardinalSplineTension:
                                                                  0.5,
                                                              xValueMapper:
                                                                  (GeneralSearchCharts
                                                                              sales,
                                                                          _) =>
                                                                      sales
                                                                          .periodDate,
                                                              yValueMapper:
                                                                  (GeneralSearchCharts
                                                                              sales,
                                                                          _) =>
                                                                      sales
                                                                          .countAcess,
                                                              dataLabelSettings:
                                                                  DataLabelSettings(
                                                                      textStyle:
                                                                          TextStyle(
                                                                        color: Constantes
                                                                            .whiteColor,
                                                                        fontWeight:
                                                                            FontWeight.bold,
                                                                      ),
                                                                      isVisible:
                                                                          true),
                                                            ),
                                                          ]),
                                                    ]),
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ),
                                        ],
                                      ),
                                    ],
                                  ),
                                  Wrap(
                                    children: [
                                      Container(
                                        color: Constantes
                                            .darkslategrayDefaultContainerColor,
                                        width: 1217,
                                        child: Column(
                                          children: [
                                            Padding(
                                              padding:
                                                  const EdgeInsets.all(6.5),
                                              child: Row(
                                                mainAxisAlignment:
                                                    MainAxisAlignment
                                                        .spaceBetween,
                                                // ignore: prefer_const_literals_to_create_immutables
                                                children: [
                                                  Icon(
                                                    Icons.search,
                                                    color:
                                                        Constantes.greenColor,
                                                  ),
                                                  Text(
                                                    'Nível de Satisfação',
                                                    style: TextStyle(
                                                      color: Constantes
                                                          .lightGrayColor,
                                                      fontSize: Constantes
                                                          .fontSizeBase,
                                                    ),
                                                  ),
                                                  InkWell(
                                                    child: Icon(
                                                      Icons
                                                          .library_add_check_outlined,
                                                      size: 20,
                                                      color: Constantes
                                                          .lightGrayColor,
                                                    ),
                                                    onTap: () {
                                                      showDialog<String>(
                                                        context: context,
                                                        builder: (BuildContext
                                                                context) =>
                                                            AlertDialog(
                                                          backgroundColor:
                                                              Constantes
                                                                  .defaultDarkslategrayColor,
                                                          title: const Text(
                                                            'Informação',
                                                            style: TextStyle(
                                                              color: Constantes
                                                                  .secondWhiteDialogColor,
                                                            ),
                                                          ),
                                                          content: const Text(
                                                            'Nível de satisfação médio, por pergunta, indicado nas pesquisas realizadas durante o período selecionado.',
                                                            style: TextStyle(
                                                              color: Constantes
                                                                  .secondWhiteDialogColor,
                                                            ),
                                                          ),
                                                          actions: <Widget>[
                                                            TextButton(
                                                              style: TextButton.styleFrom(
                                                                  backgroundColor:
                                                                      Constantes
                                                                          .greenColor),
                                                              onPressed: () =>
                                                                  Navigator.pop(
                                                                context,
                                                                'OK',
                                                              ),
                                                              child: const Text(
                                                                'OK',
                                                                style: TextStyle(
                                                                    color: Constantes
                                                                        .defaultDarkslategrayColor),
                                                              ),
                                                            ),
                                                          ],
                                                        ),
                                                      );
                                                    },
                                                  ),
                                                ],
                                              ),
                                            ),
                                            Container(
                                              width: 1217,
                                              height: 1,
                                              color: Constantes
                                                  .defaultDarkslategrayColor,
                                            ),
                                            // ignore: sized_box_for_whitespace
                                            Padding(
                                              padding: const EdgeInsets.only(
                                                  top: 75, bottom: 85),
                                              child: Container(
                                                child: Column(
                                                  crossAxisAlignment:
                                                      CrossAxisAlignment.center,
                                                  mainAxisAlignment:
                                                      MainAxisAlignment.center,
                                                  children: [
                                                    Container(
                                                      child: Wrap(
                                                        spacing: 30,
                                                        runSpacing: 45,
                                                        children:
                                                            _createListNivelContainer(),
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ],
                                  ),
                                  Wrap(
                                    spacing: 20,
                                    runSpacing: 20,
                                    children: [
                                      Wrap(
                                        children: [
                                          Container(
                                            height: 315,
                                            color: Constantes
                                                .darkslategrayDefaultContainerColor,
                                            child: Column(
                                              children: [
                                                // ignore: sized_box_for_whitespace
                                                Container(
                                                  width: 255,
                                                  child: Padding(
                                                    padding:
                                                        const EdgeInsets.only(
                                                            top: 5, bottom: 5),
                                                    child: Wrap(
                                                      alignment: WrapAlignment
                                                          .spaceBetween,
                                                      // ignore: prefer_const_literals_to_create_immutables
                                                      children: [
                                                        Icon(
                                                          Icons.search,
                                                          color: Constantes
                                                              .greenColor,
                                                        ),
                                                        Text(
                                                          'Comentários Respondidos',
                                                          style: TextStyle(
                                                            color: Constantes
                                                                .lightGrayColor,
                                                            fontSize: Constantes
                                                                .fontSizeBase,
                                                          ),
                                                        ),
                                                        InkWell(
                                                          child: Icon(
                                                            Icons
                                                                .library_add_check_outlined,
                                                            size: 20,
                                                            color: Constantes
                                                                .lightGrayColor,
                                                          ),
                                                          onTap: () {
                                                            showDialog<String>(
                                                              context: context,
                                                              builder: (BuildContext
                                                                      context) =>
                                                                  AlertDialog(
                                                                backgroundColor:
                                                                    Constantes
                                                                        .defaultDarkslategrayColor,
                                                                title:
                                                                    const Text(
                                                                  'Informação',
                                                                  style:
                                                                      TextStyle(
                                                                    color: Constantes
                                                                        .secondWhiteDialogColor,
                                                                  ),
                                                                ),
                                                                content:
                                                                    const Text(
                                                                  'Quantidade total de comentários respondidos no período selecionado.',
                                                                  style:
                                                                      TextStyle(
                                                                    color: Constantes
                                                                        .secondWhiteDialogColor,
                                                                  ),
                                                                ),
                                                                actions: <
                                                                    Widget>[
                                                                  TextButton(
                                                                    style: TextButton.styleFrom(
                                                                        backgroundColor:
                                                                            Constantes.greenColor),
                                                                    onPressed: () =>
                                                                        Navigator
                                                                            .pop(
                                                                      context,
                                                                      'OK',
                                                                    ),
                                                                    child:
                                                                        const Text(
                                                                      'OK',
                                                                      style: TextStyle(
                                                                          color:
                                                                              Constantes.defaultDarkslategrayColor),
                                                                    ),
                                                                  ),
                                                                ],
                                                              ),
                                                            );
                                                          },
                                                        ),
                                                      ],
                                                    ),
                                                  ),
                                                ),
                                                Container(
                                                  width: 255,
                                                  height: 1,
                                                  color: Constantes
                                                      .defaultDarkslategrayColor,
                                                ),
                                                // ignore: sized_box_for_whitespace
                                                Container(
                                                  alignment: Alignment.center,
                                                  height: 150,
                                                  width: 266,
                                                  child: Row(
                                                    mainAxisAlignment:
                                                        MainAxisAlignment
                                                            .center,
                                                    children: [
                                                      Text(
                                                        "$_countCommentNote",
                                                        style: TextStyle(
                                                            color: Constantes
                                                                .lightGrayColor,
                                                            fontSize: Constantes
                                                                .fontSizeBigEmphasis),
                                                        textAlign:
                                                            TextAlign.center,
                                                      ),
                                                      (_countCommentNote > 0)
                                                          ? Icon(
                                                              Icons
                                                                  .arrow_drop_up,
                                                              size: Constantes
                                                                  .iconSizeBigEmphasis,
                                                              color: Constantes
                                                                  .greenColor,
                                                            )
                                                          : Icon(
                                                              Icons
                                                                  .arrow_drop_down,
                                                              size: Constantes
                                                                  .iconSizeEmphasis,
                                                              color: Constantes
                                                                  .redColor,
                                                            ),
                                                      Text(
                                                        "100%",
                                                        style: TextStyle(
                                                          fontWeight:
                                                              FontWeight.bold,
                                                          fontSize: Constantes
                                                              .fontSizeEmphasis,
                                                          color:
                                                              (_countCommentNote <=
                                                                      0)
                                                                  ? Constantes
                                                                      .redColor
                                                                  : Constantes
                                                                      .greenColor,
                                                        ),
                                                      ),
                                                    ],
                                                  ),
                                                ),
                                                Container(
                                                  width: 255,
                                                  height: 1,
                                                  color: Constantes
                                                      .defaultDarkslategrayColor,
                                                ),
                                                Padding(
                                                  padding:
                                                      const EdgeInsets.only(
                                                          top: 35.0),
                                                  child: Container(
                                                    width: 255,
                                                    child: Column(
                                                      children: [
                                                        (_hassPressedCommentDaily !=
                                                                false)
                                                            ? Row(
                                                                mainAxisAlignment:
                                                                    MainAxisAlignment
                                                                        .spaceBetween,
                                                                children: [
                                                                  RichText(
                                                                    text:
                                                                        TextSpan(
                                                                      style:
                                                                          TextStyle(
                                                                        fontSize:
                                                                            Constantes.fontSizeBase,
                                                                        color: Constantes
                                                                            .lightGrayColor,
                                                                      ),
                                                                      // ignore: prefer_const_literals_to_create_immutables
                                                                      children: <
                                                                          TextSpan>[
                                                                        TextSpan(
                                                                            text:
                                                                                'Total '),
                                                                        TextSpan(
                                                                            text:
                                                                                'Diário',
                                                                            style:
                                                                                TextStyle(color: Constantes.lightseagreenColor)),
                                                                      ],
                                                                    ),
                                                                  ),
                                                                  Row(
                                                                    children: [
                                                                      Text(
                                                                        "${_countCommentNote}",
                                                                        style: TextStyle(
                                                                            color:
                                                                                Constantes.lightGrayColor,
                                                                            fontSize: Constantes.fontSizeBase),
                                                                        textAlign:
                                                                            TextAlign.center,
                                                                      ),
                                                                      (_countCommentNote >
                                                                              0)
                                                                          ? Icon(
                                                                              Icons.arrow_drop_up,
                                                                              size: Constantes.iconSizeBase,
                                                                              color: Constantes.greenColor,
                                                                            )
                                                                          : Icon(
                                                                              Icons.arrow_drop_down,
                                                                              size: Constantes.iconSizeBase,
                                                                              color: Constantes.redColor,
                                                                            ),
                                                                      Text(
                                                                        "100%",
                                                                        style:
                                                                            TextStyle(
                                                                          fontWeight:
                                                                              FontWeight.bold,
                                                                          fontSize:
                                                                              Constantes.fontSizeBase,
                                                                          color: (_countCommentNote <= 0)
                                                                              ? Constantes.redColor
                                                                              : Constantes.greenColor,
                                                                        ),
                                                                      ),
                                                                    ],
                                                                  ),
                                                                ],
                                                              )
                                                            : Container(),
                                                        (_hassPressedCommentYesterday !=
                                                                false)
                                                            ? Row(
                                                                mainAxisAlignment:
                                                                    MainAxisAlignment
                                                                        .spaceBetween,
                                                                children: [
                                                                  RichText(
                                                                    text:
                                                                        TextSpan(
                                                                      style:
                                                                          TextStyle(
                                                                        fontSize:
                                                                            Constantes.fontSizeBase,
                                                                        color: Constantes
                                                                            .lightGrayColor,
                                                                      ),
                                                                      // ignore: prefer_const_literals_to_create_immutables
                                                                      children: <
                                                                          TextSpan>[
                                                                        TextSpan(
                                                                            text:
                                                                                'Total '),
                                                                        TextSpan(
                                                                            text:
                                                                                'Dia Anterior',
                                                                            style:
                                                                                TextStyle(color: Constantes.lightseagreenColor)),
                                                                      ],
                                                                    ),
                                                                  ),
                                                                  Row(
                                                                    children: [
                                                                      Text(
                                                                        "${_countCommentNote}",
                                                                        style: TextStyle(
                                                                            color:
                                                                                Constantes.lightGrayColor,
                                                                            fontSize: Constantes.fontSizeBase),
                                                                        textAlign:
                                                                            TextAlign.center,
                                                                      ),
                                                                      (_countCommentNote >
                                                                              0)
                                                                          ? Icon(
                                                                              Icons.arrow_drop_up,
                                                                              size: Constantes.iconSizeBase,
                                                                              color: Constantes.greenColor,
                                                                            )
                                                                          : Icon(
                                                                              Icons.arrow_drop_down,
                                                                              size: Constantes.iconSizeBase,
                                                                              color: Constantes.redColor,
                                                                            ),
                                                                      Text(
                                                                        "100%",
                                                                        style:
                                                                            TextStyle(
                                                                          fontWeight:
                                                                              FontWeight.bold,
                                                                          fontSize:
                                                                              Constantes.fontSizeBase,
                                                                          color: (_countCommentNote <= 0)
                                                                              ? Constantes.redColor
                                                                              : Constantes.greenColor,
                                                                        ),
                                                                      ),
                                                                    ],
                                                                  ),
                                                                ],
                                                              )
                                                            : Container(),
                                                        (_hassPressedCommentLastWeek !=
                                                                false)
                                                            ? Row(
                                                                mainAxisAlignment:
                                                                    MainAxisAlignment
                                                                        .spaceBetween,
                                                                children: [
                                                                  RichText(
                                                                    text:
                                                                        TextSpan(
                                                                      style:
                                                                          TextStyle(
                                                                        fontSize:
                                                                            Constantes.fontSizeBase,
                                                                        color: Constantes
                                                                            .lightGrayColor,
                                                                      ),
                                                                      // ignore: prefer_const_literals_to_create_immutables
                                                                      children: <
                                                                          TextSpan>[
                                                                        TextSpan(
                                                                            text:
                                                                                'Total '),
                                                                        TextSpan(
                                                                            text:
                                                                                'Últimos 7 dias ',
                                                                            style:
                                                                                TextStyle(color: Constantes.lightseagreenColor)),
                                                                      ],
                                                                    ),
                                                                  ),
                                                                  Row(
                                                                    children: [
                                                                      Text(
                                                                        "${_countCommentNote}",
                                                                        style: TextStyle(
                                                                            color:
                                                                                Constantes.lightGrayColor,
                                                                            fontSize: Constantes.fontSizeBase),
                                                                        textAlign:
                                                                            TextAlign.center,
                                                                      ),
                                                                      (_countCommentNote >
                                                                              0)
                                                                          ? Icon(
                                                                              Icons.arrow_drop_up,
                                                                              size: Constantes.iconSizeBase,
                                                                              color: Constantes.greenColor,
                                                                            )
                                                                          : Icon(
                                                                              Icons.arrow_drop_down,
                                                                              size: Constantes.iconSizeBase,
                                                                              color: Constantes.redColor,
                                                                            ),
                                                                      Text(
                                                                        "100%",
                                                                        style:
                                                                            TextStyle(
                                                                          fontWeight:
                                                                              FontWeight.bold,
                                                                          fontSize:
                                                                              Constantes.fontSizeBase,
                                                                          color: (_countCommentNote <= 0)
                                                                              ? Constantes.redColor
                                                                              : Constantes.greenColor,
                                                                        ),
                                                                      ),
                                                                    ],
                                                                  ),
                                                                ],
                                                              )
                                                            : Container(),
                                                        (_hassPressedCommentLastMonth !=
                                                                false)
                                                            ? Row(
                                                                mainAxisAlignment:
                                                                    MainAxisAlignment
                                                                        .spaceBetween,
                                                                children: [
                                                                  RichText(
                                                                    text:
                                                                        TextSpan(
                                                                      style:
                                                                          TextStyle(
                                                                        fontSize:
                                                                            Constantes.fontSizeBase,
                                                                        color: Constantes
                                                                            .lightGrayColor,
                                                                      ),
                                                                      // ignore: prefer_const_literals_to_create_immutables
                                                                      children: <
                                                                          TextSpan>[
                                                                        TextSpan(
                                                                            text:
                                                                                'Total '),
                                                                        TextSpan(
                                                                            text:
                                                                                'Mensal',
                                                                            style:
                                                                                TextStyle(color: Constantes.lightseagreenColor)),
                                                                      ],
                                                                    ),
                                                                  ),
                                                                  Row(
                                                                    children: [
                                                                      Text(
                                                                        "${_countCommentNote}",
                                                                        style: TextStyle(
                                                                            color:
                                                                                Constantes.lightGrayColor,
                                                                            fontSize: Constantes.fontSizeBase),
                                                                        textAlign:
                                                                            TextAlign.center,
                                                                      ),
                                                                      (_countCommentNote >
                                                                              0)
                                                                          ? Icon(
                                                                              Icons.arrow_drop_up,
                                                                              size: Constantes.iconSizeBase,
                                                                              color: Constantes.greenColor,
                                                                            )
                                                                          : Icon(
                                                                              Icons.arrow_drop_down,
                                                                              size: Constantes.iconSizeBase,
                                                                              color: Constantes.redColor,
                                                                            ),
                                                                      Text(
                                                                        "100%",
                                                                        style:
                                                                            TextStyle(
                                                                          fontWeight:
                                                                              FontWeight.bold,
                                                                          fontSize:
                                                                              Constantes.fontSizeBase,
                                                                          color: (_accessSearchList.length <= 0)
                                                                              ? Constantes.redColor
                                                                              : Constantes.greenColor,
                                                                        ),
                                                                      ),
                                                                    ],
                                                                  ),
                                                                ],
                                                              )
                                                            : Container(),
                                                      ],
                                                    ),
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ),
                                        ],
                                      ),
                                      Wrap(
                                        children: [
                                          Column(
                                            children: [
                                              Container(
                                                color: Constantes
                                                    .darkslategrayDefaultContainerColor,
                                                width: 930,
                                                child: Column(
                                                  children: [
                                                    Padding(
                                                      padding:
                                                          const EdgeInsets.all(
                                                              6.5),
                                                      child: Row(
                                                        mainAxisAlignment:
                                                            MainAxisAlignment
                                                                .spaceBetween,
                                                        // ignore: prefer_const_literals_to_create_immutables
                                                        children: [
                                                          Icon(
                                                            Icons
                                                                .access_time_outlined,
                                                            size: 20,
                                                            color: Constantes
                                                                .whiteColor,
                                                          ),
                                                          Text(
                                                            'Perguntas',
                                                            style: TextStyle(
                                                              color: Constantes
                                                                  .lightGrayColor,
                                                              fontSize: Constantes
                                                                  .fontSizeBase,
                                                            ),
                                                          ),
                                                          InkWell(
                                                            child: Icon(
                                                              Icons
                                                                  .library_add_check_outlined,
                                                              size: 20,
                                                              color: Constantes
                                                                  .lightGrayColor,
                                                            ),
                                                            onTap: () {
                                                              showDialog<
                                                                  String>(
                                                                context:
                                                                    context,
                                                                builder: (BuildContext
                                                                        context) =>
                                                                    AlertDialog(
                                                                  backgroundColor:
                                                                      Constantes
                                                                          .defaultDarkslategrayColor,
                                                                  title:
                                                                      const Text(
                                                                    'Informação',
                                                                    style:
                                                                        TextStyle(
                                                                      color: Constantes
                                                                          .secondWhiteDialogColor,
                                                                    ),
                                                                  ),
                                                                  content:
                                                                      const Text(
                                                                    'Média de classificação indicada nas respostas conforme cada pergunta realizada durante o período selecionado.',
                                                                    style:
                                                                        TextStyle(
                                                                      color: Constantes
                                                                          .secondWhiteDialogColor,
                                                                    ),
                                                                  ),
                                                                  actions: <
                                                                      Widget>[
                                                                    TextButton(
                                                                      style: TextButton.styleFrom(
                                                                          backgroundColor:
                                                                              Constantes.greenColor),
                                                                      onPressed:
                                                                          () =>
                                                                              Navigator.pop(
                                                                        context,
                                                                        'OK',
                                                                      ),
                                                                      child:
                                                                          const Text(
                                                                        'OK',
                                                                        style: TextStyle(
                                                                            color:
                                                                                Constantes.defaultDarkslategrayColor),
                                                                      ),
                                                                    ),
                                                                  ],
                                                                ),
                                                              );
                                                            },
                                                          ),
                                                        ],
                                                      ),
                                                    ),
                                                    Container(
                                                      width: 930,
                                                      height: 1,
                                                      color: Constantes
                                                          .defaultDarkslategrayColor,
                                                    ),
                                                    Padding(
                                                      padding:
                                                          const EdgeInsets.only(
                                                              top: 10,
                                                              bottom: 10),
                                                      child: Container(
                                                        child: Column(
                                                          mainAxisAlignment:
                                                              MainAxisAlignment
                                                                  .center,
                                                          children: [
                                                            Wrap(
                                                              spacing: 20,
                                                              runSpacing: 20,
                                                              // ignore: prefer_const_literals_to_create_immutables
                                                              children: [
                                                                // ignore: avoid_unnecessary_containers
                                                                Container(
                                                                  child: Column(
                                                                    children: [
                                                                      Container(
                                                                        color: Constantes
                                                                            .defaultDarkslategrayColor,
                                                                        height:
                                                                            35,
                                                                        width:
                                                                            275,
                                                                        child:
                                                                            Row(
                                                                          mainAxisAlignment:
                                                                              MainAxisAlignment.center,
                                                                          // ignore: prefer_const_literals_to_create_immutables
                                                                          children: [
                                                                            Text(
                                                                              'Pergunta 1',
                                                                              textAlign: TextAlign.center,
                                                                              style: TextStyle(color: Constantes.lightGrayColor, fontSize: 20),
                                                                            ),
                                                                          ],
                                                                        ),
                                                                      ),
                                                                      Container(
                                                                        height:
                                                                            230,
                                                                        width:
                                                                            275,
                                                                        color: Constantes
                                                                            .darkslategrayBlocksQuestions,
                                                                        child:
                                                                            Padding(
                                                                          padding: const EdgeInsets.only(
                                                                              top: 10,
                                                                              bottom: 20),
                                                                          child:
                                                                              Column(
                                                                            mainAxisAlignment:
                                                                                MainAxisAlignment.spaceBetween,
                                                                            children: [
                                                                              Text(
                                                                                'Atendimentos dos Colaboradores',
                                                                                textAlign: TextAlign.center,
                                                                                style: TextStyle(fontSize: 22, color: Constantes.lightGrayColor),
                                                                              ),
                                                                              Row(
                                                                                mainAxisAlignment: MainAxisAlignment.center,
                                                                                children: _createListStarsButtons(),
                                                                              ),
                                                                            ],
                                                                          ),
                                                                        ),
                                                                      ),
                                                                    ],
                                                                  ),
                                                                ),
                                                                // ignore: avoid_unnecessary_containers
                                                                Container(
                                                                  child: Column(
                                                                    children: [
                                                                      Container(
                                                                        color: Constantes
                                                                            .defaultDarkslategrayColor,
                                                                        height:
                                                                            35,
                                                                        width:
                                                                            275,
                                                                        child:
                                                                            Row(
                                                                          mainAxisAlignment:
                                                                              MainAxisAlignment.center,
                                                                          // ignore: prefer_const_literals_to_create_immutables
                                                                          children: [
                                                                            Text(
                                                                              'Pergunta 2',
                                                                              textAlign: TextAlign.center,
                                                                              style: TextStyle(color: Constantes.lightGrayColor, fontSize: 20),
                                                                            ),
                                                                          ],
                                                                        ),
                                                                      ),
                                                                      Container(
                                                                        height:
                                                                            230,
                                                                        width:
                                                                            275,
                                                                        color: Constantes
                                                                            .darkslategrayBlocksQuestions,
                                                                        child:
                                                                            Padding(
                                                                          padding: const EdgeInsets.only(
                                                                              top: 10,
                                                                              bottom: 20),
                                                                          child:
                                                                              Column(
                                                                            mainAxisAlignment:
                                                                                MainAxisAlignment.spaceBetween,
                                                                            children: [
                                                                              Text(
                                                                                'Ambiente do Posto de Atendimento',
                                                                                textAlign: TextAlign.center,
                                                                                style: TextStyle(fontSize: 22, color: Constantes.lightGrayColor),
                                                                              ),
                                                                              Row(
                                                                                mainAxisAlignment: MainAxisAlignment.center,
                                                                                children: _createSecondListStarsButtons(),
                                                                              ),
                                                                            ],
                                                                          ),
                                                                        ),
                                                                      ),
                                                                    ],
                                                                  ),
                                                                ),
                                                                // ignore: avoid_unnecessary_containers
                                                                Container(
                                                                  child: Column(
                                                                    children: [
                                                                      Container(
                                                                        color: Constantes
                                                                            .defaultDarkslategrayColor,
                                                                        height:
                                                                            35,
                                                                        width:
                                                                            275,
                                                                        child:
                                                                            Row(
                                                                          mainAxisAlignment:
                                                                              MainAxisAlignment.center,
                                                                          // ignore: prefer_const_literals_to_create_immutables
                                                                          children: [
                                                                            Text(
                                                                              'Pergunta 3',
                                                                              textAlign: TextAlign.center,
                                                                              style: TextStyle(color: Constantes.lightGrayColor, fontSize: 20),
                                                                            ),
                                                                          ],
                                                                        ),
                                                                      ),
                                                                      Container(
                                                                        height:
                                                                            230,
                                                                        width:
                                                                            275,
                                                                        color: Constantes
                                                                            .darkslategrayBlocksQuestions,
                                                                        child:
                                                                            Padding(
                                                                          padding: const EdgeInsets.only(
                                                                              top: 10,
                                                                              bottom: 20),
                                                                          child:
                                                                              Column(
                                                                            mainAxisAlignment:
                                                                                MainAxisAlignment.spaceBetween,
                                                                            children: [
                                                                              Text(
                                                                                'Tempo de espera',
                                                                                textAlign: TextAlign.center,
                                                                                style: TextStyle(fontSize: 22, color: Constantes.lightGrayColor),
                                                                              ),
                                                                              Row(
                                                                                mainAxisAlignment: MainAxisAlignment.center,
                                                                                children: _createThirdListStarsButtons(),
                                                                              ),
                                                                            ],
                                                                          ),
                                                                        ),
                                                                      ),
                                                                    ],
                                                                  ),
                                                                ),
                                                              ],
                                                            ),
                                                          ],
                                                        ),
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                              ),
                                            ],
                                          ),
                                        ],
                                      ),
                                    ],
                                  ),
                                ],
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
