import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:viacredi_dashboard/screens/dashboard_home.dart';
import 'package:viacredi_dashboard/screens/login.dart';
//import 'package:firebase_core/firebase_core.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  //await Firebase.initializeApp();
  await Firebase.initializeApp(
      // ignore: prefer_const_literals_to_create_immutables
      options: FirebaseOptions.fromMap({
    "apiKey": "AIzaSyDBN_Zjkux_MxfNjIeev2fOX9iIxDl5qnE",
    "authDomain": "viacredi-vex.firebaseapp.com",
    "projectId": "viacredi-vex",
    "storageBucket": "viacredi-vex.appspot.com",
    "messagingSenderId": "842610537388",
    "appId": "1:842610537388:web:150110ed86816decf4c009"
  }));
  //await Firebase.initializeApp();
  runApp(ViacrediDashboard());
}

class ViacrediDashboard extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      routes: <String, WidgetBuilder>{
        '/': (BuildContext context) => Login(),
        '/DashboardHome': (BuildContext context) => DashboardHome(),
      },
      theme: ThemeData(
        textTheme: GoogleFonts.robotoCondensedTextTheme(),
      ),
      debugShowCheckedModeBanner: false,
      title: 'Viacredi',
      initialRoute: '/',
    );
  }
}
