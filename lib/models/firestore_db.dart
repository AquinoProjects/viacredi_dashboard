import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:firebase_core/firebase_core.dart';

class FireStoreDb {
  final CollectionReference collectionRef =
      FirebaseFirestore.instance.collection('search');

  Future<List> getData(int? ag, String? mac, int startDate, int endDate) async {
    List searchList = [];
    print("startDate: " + startDate.toString());
    print("endDate: " + endDate.toString());
    try {
      var query = collectionRef
          .where("date", isGreaterThanOrEqualTo: startDate)
          .where("date", isLessThanOrEqualTo: endDate);

      if (mac != "") {
        query = query.where("mac", isEqualTo: mac);
      }

      if (ag != 0) {
        query = query.where("agency", isEqualTo: ag);
      }

      var retQuery = await query.get();

      for (var result in retQuery.docs) {
        searchList.add(result.data());
      }
      print("t: " + searchList.length.toString());
      return searchList;
    } catch (e) {
      print("Error - $e");
      return searchList;
    }
  }
}
