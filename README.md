

#              Viacredi Dashboard
Projeto desenvolvido como desafio pela empresa Grupo Vex, para entendimento das tecnologias que foram usadas.
Para abrir faça um clone do projeto e na raiz execute o comando "flutter run" usando o devtols como dispositivo de execução.
Os dados usados de resultado são preenchidos no outro projeto vinculado a dash, se quiser dar uma olhada, acesse o repositório viacredi-vex.

Você também pode abrir o projeto com o link. use CTRL+click no: https://viacredi-vex.web.app/#/
e acese com login: 'bitbucket@teste.com.br' senha: 'visitante123'. 
# Tecnologias Usadas
🔥Dart
🔥Flutter
🔥Firestore
